package com.anhttvn.neymarpsgwallpaper.util;

import android.Manifest;
import android.app.ProgressDialog;
import android.app.WallpaperManager;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.anhttvn.neymarpsgwallpaper.R;
import com.anhttvn.neymarpsgwallpaper.database.DatabaseHandler;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.annotations.NotNull;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public abstract class BaseActivity extends AppCompatActivity {

  protected Bundle savedInstanceState;
  protected DatabaseReference databaseReference;
  protected DatabaseHandler db;
  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    savedInstanceState = savedInstanceState;
    db = new DatabaseHandler(this);
    setContentView(contentView());
    if (savedInstanceState != null) {
      savedInstanceState.clear();
    }
    init();
    configFullADS();
    this.createFolder();
  }

  public abstract void init();
  public abstract View contentView();

  /**
   * show ads banner
   */
  public void isBannerADS (AdView ads) {
    AdRequest adRequest = new AdRequest.Builder().build();
    if (isConnected()) {
      ads.setVisibility(View.VISIBLE);
      ads.loadAd(adRequest);
    }else{
      ads.setVisibility(View.GONE);
    }
  }

  private InterstitialAd mInterstitialAd;
  private AdRequest mAdRequest;

  protected void configFullADS() {
    FullScreenContentCallback fullScreenContentCallback = new FullScreenContentCallback() {
      @Override
      public void onAdDismissedFullScreenContent() {
        mInterstitialAd = null;
      }
    };

    InterstitialAd.load(
            this,
            this.getString(R.string.banner_full_screen),
            new AdRequest.Builder().build(),
            new InterstitialAdLoadCallback() {
              @Override
              public void onAdLoaded(@NonNull InterstitialAd ad) {
                mInterstitialAd = ad;
                mInterstitialAd.setFullScreenContentCallback(fullScreenContentCallback);
              }

              @Override
              public void onAdFailedToLoad(@NonNull LoadAdError adError) {
                // Code to be executed when an ad request fails.
              }
            });

  }

  protected void isADSFull() {
    if (mInterstitialAd != null) {
      mInterstitialAd.show(this);
    }
  }

  protected boolean isConnected() {
    return Connectivity.isConnectedFast(this);
  }

  protected void showToast(String mToastMsg) {
    Toast.makeText(this, mToastMsg, Toast.LENGTH_LONG).show();
  }

  protected void homeWall(Bitmap bm){
    DisplayMetrics metrics = new DisplayMetrics();
    getWindowManager().getDefaultDisplay().getMetrics(metrics);
    int height = metrics.heightPixels;
    int width = metrics.widthPixels;
    Bitmap bitmap = Bitmap.createScaledBitmap(bm,width,height, true);
    WallpaperManager wallpaperManager = WallpaperManager.getInstance(this);

    wallpaperManager.setWallpaperOffsetSteps(1, 1);
    wallpaperManager.suggestDesiredDimensions(width, height);
    try {
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        wallpaperManager.setBitmap(bitmap, null, true, WallpaperManager.FLAG_SYSTEM);
      } else {
        wallpaperManager.setBitmap(bitmap);
      }
      Toast.makeText(this, getResources().getString(R.string.homeSuccessfully), Toast.LENGTH_SHORT).show();
    } catch (IOException e) {
      e.printStackTrace();

    }
  }
  protected void lockWall(Bitmap bm){
    DisplayMetrics metrics = new DisplayMetrics();
    getWindowManager().getDefaultDisplay().getMetrics(metrics);
    int height = metrics.heightPixels;
    int width = metrics.widthPixels;
    Bitmap bitmap = Bitmap.createScaledBitmap(bm,width,height, true);
    WallpaperManager wallpaperManager = WallpaperManager.getInstance(this);
    wallpaperManager.setWallpaperOffsetSteps(1, 1);
    wallpaperManager.suggestDesiredDimensions(width, height);
    try {
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        wallpaperManager.setBitmap(bitmap, null, true, WallpaperManager.FLAG_LOCK);
      } else {
        wallpaperManager.setBitmap(bitmap);
      }
      Toast.makeText(this, getResources().getString(R.string.lockSuccessfully), Toast.LENGTH_SHORT).show();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  protected void downloadImage(String pathURL, final String name) {
    final ProgressDialog dialog = new ProgressDialog(this);
    dialog.setMessage(getString(R.string.please_download));
    dialog.show();
    Picasso.with(this)
            .load(pathURL)
            .into(new Target() {
              @Override
              public void onBitmapLoaded (final Bitmap bitmap, Picasso.LoadedFrom from){
                try {

                  File myDir = new File(getExternalFilesDir() + File.separator + Config.FOLDER_DOWNLOAD);

                  if (!myDir.exists()) {
                    myDir = new File(getExternalStorageDirectory() +File.separator+ Config.FOLDER_DOWNLOAD);
                    if (!myDir.exists()) {
                      askPermission();
                    }
                  }

                  String nameFile = name + ".jpg";
                  myDir = new File(myDir, nameFile);
                  FileOutputStream out = new FileOutputStream(myDir);
                  bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                  new android.os.Handler().postDelayed(
                          () -> {
                            dialog.dismiss();
                            showToast(getString(R.string.download_success));
                          },
                          1000);

                  out.flush();
                  out.close();

                } catch(Exception e){
                  // some action
                }
              }

              @Override
              public void onBitmapFailed(Drawable errorDrawable) {

              }

              @Override
              public void onPrepareLoad(Drawable placeHolderDrawable) {

              }
            });
  }

  private String getExternalStorageDirectory() {
    String baseDir = Environment.getExternalStorageDirectory().getAbsolutePath();
    return baseDir != null && baseDir.length() > 0 ?  baseDir : null;
  }

  private  File getExternalFilesDir() {
    File path = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
    return path.getAbsolutePath() != null ? path : null;
  }


  private void askPermission() {

    ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, Config.PERMISSION_REQUEST_CODE);
  }

  private void createFolder() {
    if (ContextCompat.checkSelfPermission(BaseActivity.this,Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
      createDirectory();

    }else{
      askPermission();
    }
  }

  private void createDirectory() {
    if (getExternalFilesDir() != null) {
      File folder = new File(getExternalFilesDir(), File.separator + Config.FOLDER_DOWNLOAD);
      if (!folder.exists()) {
        folder.mkdirs();
      }
    }else  {
      File myDir = new File(getExternalStorageDirectory() +File.separator+ Config.FOLDER_DOWNLOAD);

      if (!myDir.exists()) {
        myDir.mkdirs();
      }

    }
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull @NotNull String[] permissions, @NonNull @NotNull int[] grantResults) {

    if (requestCode == Config.PERMISSION_REQUEST_CODE)
    {
      if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

        createDirectory();
      }else {
        Toast.makeText(BaseActivity.this,"Permission Denied",Toast.LENGTH_SHORT).show();
      }

    }

    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
  }


  protected String commonTitleHeader(Config.KEY key) {
    String title = "";
    switch (key) {
      case WALLPAPER:
        title = this.getString(R.string.wallpaper);
        break;
      case DOWNLOAD:
        title = this.getString(R.string.download);
        break;
      case FAVORITE:
        title = this.getString(R.string.favorite);
        break;

      case NOTIFICATION:
        title = this.getString(R.string.notification);
        break;

    }

    return title;
  }

  protected String commonNumber(int number) {
    return number < 1000 ? String.valueOf(number) : (number / 1000) + "K+";
  }


  public Bitmap drawableToBitmap(Drawable drawable) {
    if (drawable instanceof BitmapDrawable) {
      return ((BitmapDrawable) drawable).getBitmap();
    }

    // We ask for the bounds if they have been set as they would be most
    // correct, then we check we are  > 0
    final int width = !drawable.getBounds().isEmpty() ?
            drawable.getBounds().width() : drawable.getIntrinsicWidth();

    final int height = !drawable.getBounds().isEmpty() ?
            drawable.getBounds().height() : drawable.getIntrinsicHeight();

    // Now we check we are > 0
    final Bitmap bitmap = Bitmap.createBitmap(width <= 0 ? 1 : width, height <= 0 ? 1 : height,
            Bitmap.Config.ARGB_8888);
    Canvas canvas = new Canvas(bitmap);
    drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
    drawable.draw(canvas);

    return bitmap;
  }

  protected ArrayList<String> getWallpaperFolder() {
    ArrayList<String> list = new ArrayList<>();
    String path =
            getExternalFilesDir() != null ? getExternalFilesDir() +"/" +File.separator + "/" + Config.FOLDER_DOWNLOAD :
                    getExternalStorageDirectory() +File.separator+ Config.FOLDER_DOWNLOAD;
    if (path == null) {
      return list;
    }
    File root =new File(path);
    File[] files= root.listFiles();
    if (files == null) {
      return list;
    }
    list.clear();
    for (File file: files){
      list.add(file.getPath());
    }

    return list;
  }
}

