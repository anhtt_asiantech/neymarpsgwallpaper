package com.anhttvn.neymarpsgwallpaper.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.anhttvn.neymarpsgwallpaper.R;
import com.anhttvn.neymarpsgwallpaper.model.Wallpaper;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Locale;

public class WallpaperAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {

  private List<Wallpaper> wallpapers;
  private Context context;
  private EventOnclick eventOnclick;

  private static final int LAYOUT_ADS = 1;
  private static final int LAYOUT_TYPE = 3;

  public WallpaperAdapter(Context ctx, List<Wallpaper> list, EventOnclick click) {
    context = ctx;
    wallpapers = list;
    eventOnclick = click;
  }
  @NonNull
  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    if (viewType == LAYOUT_ADS) {
      View view = LayoutInflater.from(context)
              .inflate(R.layout.ads,parent,false);
      return new AdsViewHolder(view);
    } else {
      View view = LayoutInflater.from(context)
              .inflate(R.layout.adapter_wallpaper,parent,false);
      WallpaperViewHolder itemView = new WallpaperViewHolder(view);
      return itemView;
    }
  }

  @Override
  public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
    if (holder.getItemViewType() == LAYOUT_ADS) {
      AdsViewHolder holderAds = (AdsViewHolder) holder;
      isBannerADS(holderAds.ads);
    } else {
      Wallpaper wallpaper = wallpapers.get(position);
      WallpaperViewHolder wallpaperViewHolder = (WallpaperViewHolder) holder;
      if (wallpaper != null) {
        if ( Locale.getDefault().getDisplayLanguage().equalsIgnoreCase("vn")) {
          wallpaperViewHolder.title.setText(wallpaper.getTitleVn());
        } else {
          wallpaperViewHolder.title.setText(wallpaper.getTitleEn());
        }

        if (wallpaper.getImage() == null || wallpaper.getImage().isEmpty()) {
          wallpaperViewHolder.image.setImageResource(R.drawable.ic_no_thumbnail);
        } else {
          Picasso.with(context).load(wallpaper.getImage())
                  .placeholder(R.drawable.ic_no_thumbnail)
                  .error(R.drawable.ic_no_thumbnail)
                  .into(wallpaperViewHolder.image);
        }

        wallpaperViewHolder.total.setText(String.valueOf(wallpaper.getTotal()));
        wallpaperViewHolder.date.setText(wallpaper.getDate());

        wallpaperViewHolder.card.setOnClickListener(this);
        wallpaperViewHolder.card.setTag(position);

      }
    }
  }

  @Override
  public int getItemCount() {
    return wallpapers.size();
  }

  @Override
  public int getItemViewType(int position) {
    return wallpapers.get(position).isAds() ? LAYOUT_ADS : LAYOUT_TYPE;
  }

  @Override
  public void onClick(View view) {
    int position = Integer.parseInt(view.getTag()+"");
    switch (view.getId()){
      case R.id.card:
        notifyDataSetChanged();
        eventOnclick.onClick(position);
        break;

    }
  }

  public class WallpaperViewHolder extends RecyclerView.ViewHolder {

    private ImageView image;
    private TextView title, total, date;
    private CardView card;
    public WallpaperViewHolder(@NonNull View itemView) {
      super(itemView);
      image = itemView.findViewById(R.id.image);
      title = itemView.findViewById(R.id.title);
      total = itemView.findViewById(R.id.tvNumberWallpaper);
      date = itemView.findViewById(R.id.tvDateWallpaper);
      card = itemView.findViewById(R.id.card);
    }
  }

  public class AdsViewHolder extends RecyclerView.ViewHolder {
    private AdView ads;

    public AdsViewHolder(@NonNull View itemView) {
      super(itemView);
      ads = itemView.findViewById(R.id.ads);
    }
  }

  public interface EventOnclick {
    void onClick(int position);
  }

  public void isBannerADS (AdView ads) {
    AdRequest adRequest = new AdRequest.Builder()
            .build();
    ads.loadAd(adRequest);
  }
}
