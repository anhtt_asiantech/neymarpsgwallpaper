package com.anhttvn.neymarpsgwallpaper.ui;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;


import androidx.appcompat.widget.PopupMenu;

import com.anhttvn.neymarpsgwallpaper.R;
import com.anhttvn.neymarpsgwallpaper.databinding.ActivitySetBinding;
import com.anhttvn.neymarpsgwallpaper.model.Photo;
import com.anhttvn.neymarpsgwallpaper.model.Wallpaper;
import com.anhttvn.neymarpsgwallpaper.util.BaseActivity;
import com.anhttvn.neymarpsgwallpaper.util.Config;
import com.anhttvn.neymarpsgwallpaper.util.MessageEvent;
import com.google.android.gms.ads.AdView;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class SetWallpaperActivity extends BaseActivity implements PopupMenu.OnMenuItemClickListener {

  private ActivitySetBinding setBinding;
  protected Photo photo;
  protected ProgressDialog dialog;
  private String pathWallpaperOffline;
  private Bitmap bitmapWallpaperOffline;
  private boolean flagTypeWallpaper = false;

  @Override
  public void init() {
    databaseReference = FirebaseDatabase.getInstance().getReference(Config.KEY_WALLPAPER);
    this.getDataIntent();
    this.showItem();
    isBannerADS(setBinding.ads);
  }

  @Override
  public View contentView() {
    setBinding = ActivitySetBinding.inflate(getLayoutInflater());
    return setBinding.getRoot();
  }

  protected void showItem() {
    setBinding.menu.setOnClickListener(v -> {
      setBinding.menu.setImageResource(R.drawable.ic_round_close);
      PopupMenu popup = new PopupMenu(this, v);
      popup.setOnMenuItemClickListener(SetWallpaperActivity.this);
      popup.inflate(R.menu.popup_menu);
      popup.show();

      popup.setOnDismissListener(menu -> {
        setBinding.menu.setImageResource(R.drawable.ic_menu);
      });
    });

    setBinding.download.setOnClickListener(v -> {
      downloadImage(photo.getPath(), photo.getId());
    });

    setBinding.end.setOnClickListener(v -> {
      onBackPressed();
    });

    setBinding.like.setOnClickListener(v -> {
      if (db.isLike(this.photo.getId())) {
        setBinding.like.setImageResource(R.drawable.ic_like_visible);
      } else {
        setBinding.like.setImageResource(R.drawable.ic_like_disable);
      }
      updateViewOrLike(photo, "like");
    });
  }

  @Override
  public boolean onMenuItemClick(MenuItem item) {
    int id = item.getItemId();
    switch(id) {
      case R.id.home:
        question(getString(R.string.home_screen), getString(R.string.question_set_home), "Home");
        return true;
      case R.id.lock:
        question(getString(R.string.lock_screen), getString(R.string.question_set_lock), "Lock");
        return true;
      case R.id.both:
        question(getString(R.string.home_lock_screen), getString(R.string.question_set_home_lock), "Both");
        return true;
      default:
        return false;
    }
  }

  private void getDataIntent() {
    Bundle bundle = getIntent().getExtras();
    if (bundle == null) {
      return;
    }
    Config.WALLPAPER typeWallpaper = (Config.WALLPAPER) bundle.getSerializable(Config.KEY_HOME);
    if (typeWallpaper == Config.WALLPAPER.ONLINE) {
      this.flagTypeWallpaper = true;
      setBinding.like.setVisibility(View.VISIBLE);
      setBinding.numberLike.setVisibility(View.VISIBLE);
      setBinding.view.setVisibility(View.VISIBLE);
      setBinding.numberView.setVisibility(View.VISIBLE);
      setBinding.download.setVisibility(View.VISIBLE);
      this.loadWallpaperOnline(bundle);
    } else {
      setBinding.like.setVisibility(View.INVISIBLE);
      setBinding.numberLike.setVisibility(View.INVISIBLE);
      setBinding.view.setVisibility(View.INVISIBLE);
      setBinding.numberView.setVisibility(View.INVISIBLE);
      setBinding.download.setVisibility(View.INVISIBLE);
      this.flagTypeWallpaper = false;
      this.loadWallpaperOffline(bundle);
    }


  }

  private void loadWallpaperOnline(Bundle bundle) {
    ProgressDialog loadWallpaper = new ProgressDialog(this);
    loadWallpaper.setMessage(getString(R.string.loadWallpaper));
    loadWallpaper.show();
    photo = (Photo) bundle.getSerializable(Config.KEY_BIND_PHOTO);
    if (photo != null) {
      if (db.isLike(this.photo.getId())) {
        setBinding.like.setImageResource(R.drawable.ic_like_disable);
      } else {
        setBinding.like.setImageResource(R.drawable.ic_like_visible);
      }
      setBinding.numberLike.setText(this.commonNumber(photo.getLike()));
      setBinding.numberView.setText(this.commonNumber(photo.getView() + 1));
      updateViewOrLike(photo, "view");
      Picasso.with(getApplicationContext()).load(photo.getPath())
              .placeholder(R.drawable.ic_no_thumbnail)
              .error(R.drawable.ic_no_thumbnail)
              .into(setBinding.imgWallpaper, new Callback() {
                @Override
                public void onSuccess() {
                  loadWallpaper.dismiss();
                }

                @Override
                public void onError() {
                  loadWallpaper.dismiss();
                  showToast(getString(R.string.notLoadImage));
                }
              });
    }
  }

  private void loadWallpaperOffline(Bundle bundle) {
    pathWallpaperOffline = (String)bundle.getSerializable(Config.KEY_PATH_WALLPAPER_OFFLINE);
    Config.TYPE_WALLPAPER type = (Config.TYPE_WALLPAPER)bundle.getSerializable(Config.KEY_WALLPAPER_OFFLINE);

    if (pathWallpaperOffline == null || pathWallpaperOffline.isEmpty()) {
      return;
    }
    if (type == Config.TYPE_WALLPAPER.GALLERY) {
      InputStream inputstream= null;
      try {
        inputstream = getApplicationContext().getAssets().open(pathWallpaperOffline);
      } catch (IOException e) {
        e.printStackTrace();
      }
      Drawable drawable = Drawable.createFromStream(inputstream, null);
      bitmapWallpaperOffline = drawableToBitmap(drawable);
      setBinding.imgWallpaper.setImageDrawable(drawable);
    } else {
      File imgFile = new File(pathWallpaperOffline);
      if(imgFile.exists())
      {
        bitmapWallpaperOffline = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
        setBinding.imgWallpaper.setImageBitmap(bitmapWallpaperOffline);
      }
    }
  }

  private void updateViewOrLike(Photo photo, String type) {

    databaseReference.child(photo.getId()).get().addOnCompleteListener(task -> {
      if (task.isSuccessful()) {
        Photo wallpaperUpdate = task.getResult().getValue(Photo.class);

        int viewNumber;
        int likeNumber;
        if (type.equalsIgnoreCase("view")) {
          viewNumber = wallpaperUpdate.getView() + 1;
          photo.setView(viewNumber);
        } else if (type.equalsIgnoreCase("like")) {
          if (db.isLike(photo.getId())) {
            photo.setFavorite(false);
            likeNumber = wallpaperUpdate.getLike() - 1;
          } else {
            photo.setFavorite(true);
            likeNumber = wallpaperUpdate.getLike() + 1;
          }
          photo.setLike(likeNumber);
        }


        databaseReference.child(wallpaperUpdate.getId()).child(type)
                .setValue(type.equalsIgnoreCase("view") ? photo.getView() : photo.getLike());
        db.addWallpaper(photo, false);
        setBinding.numberView.setText(this.commonNumber(photo.getView()));
        setBinding.numberLike.setText(this.commonNumber(photo.getLike()));
      }
      else {
        Log.e("firebase", "Error getting data", task.getException());

      }
    });

  }

  private void setWallpaper(String type) {
    dialog = new ProgressDialog(this);
    dialog.setMessage(getString(R.string.please_set_wallpaper));
    dialog.show();
    if (flagTypeWallpaper) {
      Picasso.with(this)
              .load(photo.getPath())
              .into(new Target() {
                @Override
                public void onBitmapLoaded (final Bitmap bitmap, Picasso.LoadedFrom from){
                  if (type.equalsIgnoreCase("Home")) {
                    homeWall(bitmap);
                  } else if (type.equalsIgnoreCase("Both")) {
                    homeWall(bitmap);
                    lockWall(bitmap);
                  } else {
                    lockWall(bitmap);
                  }
                  dialog.dismiss();
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                  dialog.dismiss();
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                }
              });
    } else {
      if (type.equalsIgnoreCase("Home")) {
        new Handler().postDelayed(
                () -> {
                  homeWall(bitmapWallpaperOffline);
                  dialog.dismiss();
                },
                1000);
      } else if (type.equalsIgnoreCase("Both")) {
        new Handler().postDelayed(
                () -> {
                  homeWall(bitmapWallpaperOffline);
                  lockWall(bitmapWallpaperOffline);
                  dialog.dismiss();
                },
                1000);
      } else {
        new Handler().postDelayed(
                () -> {
                  lockWall(bitmapWallpaperOffline);
                  dialog.dismiss();
                },
                1000);
      }

    }

  }


  private void question(String title, String message, String type) {
    AlertDialog.Builder dialogBuilder =	new AlertDialog.Builder(this);
    LayoutInflater inflater	= this.getLayoutInflater();
    View dialogView	= inflater.inflate(R.layout.question, null);
    TextView tvTitle = dialogView.findViewById(R.id.exit);
    tvTitle.setText(title);
    TextView tvMessage = dialogView.findViewById(R.id.message);
    tvMessage.setText(message);
    dialogBuilder.setView(dialogView);
    AdView ads = dialogView.findViewById(R.id.ads);
    ads.setVisibility(View.GONE);
    if (isConnected()) {
      ads.setVisibility(View.VISIBLE);
      isBannerADS(ads);
    }
    AlertDialog b = dialogBuilder.create();
    dialogView.findViewById(R.id.btnNo).setOnClickListener(v -> {
      b.dismiss();
    });
    dialogView.findViewById(R.id.btnYes).setOnClickListener(v -> {
      b.dismiss();
      setWallpaper(type);
    });
    b.show();
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    if(dialog != null && dialog.isShowing()) {
      dialog.dismiss();
      dialog = null;
    }
  }

  @Override
  public void onBackPressed() {
    if (flagTypeWallpaper) {
      EventBus.getDefault().postSticky(new MessageEvent(Config.KEY_UPDATE));
      isADSFull();
    }

    finish();
    super.onBackPressed();
  }
}
